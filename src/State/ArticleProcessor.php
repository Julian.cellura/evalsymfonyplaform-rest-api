<?php

namespace App\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Article;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

class ArticleProcessor implements ProcessorInterface
{
    private $_entityManager;
    private $_slugger;
    public function __construct(EntityManagerInterface $entityManager, SluggerInterface $slugger)
    {
        $this->_entityManager = $entityManager;
        $this->_slugger = $slugger;
    }
    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): void
    {
        $data->setPublishedAt(new \Datetime("now"));
        $data->setUpdatedAt(new \Datetime("now"));

        $data->setSlug($this->_slugger->slug(strtolower($data->getName()))."-".uniqid());
        $this->_entityManager->persist($data);
        $this->_entityManager->flush();
    }
    
}
