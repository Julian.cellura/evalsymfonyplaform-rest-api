<?php

namespace App\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserProcessor implements ProcessorInterface
{
    private $_entityManager;
    private $_hashedPassword;
    public function __construct(EntityManagerInterface $entityManager, UserPasswordHasherInterface $hashedPassword)
    {
        $this->_entityManager = $entityManager;
        $this->_hashedPassword = $hashedPassword;
    }
    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): void
    {
        $user = new User();
        $user->setEmail($data->getEmail());
        $user->setRoles($data->getRoles());
        $plaintextPassword = $data->getPassword();

        $hashedPassword = $this->_hashedPassword->hashPassword(
            $user,
            $plaintextPassword
        );
        $user->setPassword($hashedPassword);

        $this->_entityManager->persist($user);
        $this->_entityManager->flush();
    }
}
